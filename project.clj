(defproject myfitnessgurus "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [lib-noir "0.3.1"]
                 [compojure "1.1.3"]
                 [hiccup "1.0.2"]
                 [ring/ring-jetty-adapter "1.1.0"]
                 [bultitude "0.1.7"]
                 [com.taoensso/timbre "1.1.0"]
                 [com.taoensso/tower "1.0.0"]
                 [com.datomic/datomic-free "0.8.3731"]
                 [faker "0.2.2"]
                 [markdown-clj "0.9.13"]]
  :plugins [[lein-ring "0.8.7"]]
  :ring {:handler myfitnessgurus.handler/war-handler
         :init myfitnessgurus.handler/init}
  :main myfitnessgurus.server
  :profiles
  {:dev {:dependencies [[ring-mock "0.1.3"]
                        [ring/ring-devel "1.1.0"]]}})
