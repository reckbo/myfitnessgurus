(ns myfitnessgurus.routes.home
  (:use compojure.core)
  (:require [myfitnessgurus.views.layout :as layout]
            [myfitnessgurus.models.core :as m]
            [clojure.string :as string]
            ))


(defn instructor-row [x]
  [:tr 
   [:td (:person/firstname x) " " (:person/lastname x) 
    [:br] 
    [:small (:instructor/gym x)]] 

   [:td 
    [:span.stars (m/get-avg-rating x)]
    [:span.votes (str "&nbsp;&nbsp(" (count (m/get-ratings x)) ")")]
    ]])
  

(defn home-page [] 
  (layout/common
    [:div.page-header 
     [:h1 "Hello World! " [:small "Your guid"]]]
    ;(repeat 100 [:p "asdfasfd"])
    ;]
    ;]]

    ;[:div.hero-unit
     ;[:h1 "asdf"]
     ;[:p "blah"]
     ;[:p "blah"]
     ;]

    [:div.row
     [:div.span6
      [:div.well
       [:h2 "Instructors"]
       [:table.table
        (map instructor-row (m/get-all-instructors))
        ]
        ]]

     [:div.span6 
      [:div.well
       [:h2 "Gyms"]
       [:ul
        [:li "ins 1"]
        [:li "ins 2"]
        [:li "ins3"]
        ]]]]
    ))


(defn about-page []
  (layout/common
    [:div.page-header
     [:h1 "About"]]
   "this is the story of myfitnessgurus... work in progress"))


(defroutes home-routes 
  (GET "/" [] (home-page))
  (GET "/about" [] (about-page)))
