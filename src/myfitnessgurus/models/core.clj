(ns myfitnessgurus.models.core)
(require 
  '[datomic.api :refer [q db] :as d]
  '[faker.name]
  '[faker.internet]
  '[faker.lorem]
  '[myfitnessgurus.util :as util :refer [defpp]]
  )

(def schema 
  [
   {:db/id #db/id[:db.part/db]
    :db/ident :user/email
    :db/unique :db.unique/value
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "user email"
    :db.install/_attribute :db.part/db}

   ;{:db/id #db/id[:db.part/db]
    ;:db/ident :user/password
    ;:db/valueType :db.type/string
    ;:db/cardinality :db.cardinality/one
    ;:db/doc "user password"
    ;:db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :person/firstname
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "A person's first name"
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :person/lastname
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "A person's last name"
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :user/ratings
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/many
    :db/doc "All user's ratings"
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :instructor/gym
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db/doc "Instructor's gym"
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :rating/value
    :db/valueType :db.type/long
    :db/cardinality :db.cardinality/one
    :db/doc "The rating's score/number of stars"
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :rating/instructor
    :db/valueType :db.type/ref
    :db/cardinality :db.cardinality/one
    :db.install/_attribute :db.part/db}

   {:db/id #db/id[:db.part/db]
    :db/ident :rating/comments
    :db/valueType :db.type/string
    :db/cardinality :db.cardinality/one
    :db.install/_attribute :db.part/db}

   ])


(defn generate-instructors [n]
  (mapv #(hash-map :person/firstname % 
                   :person/lastname %2 
                   :instructor/gym (str %3 " Gym")
                   :db/id %4)
        (repeatedly n faker.name/first-name)
        (repeatedly n faker.name/last-name)
        (repeatedly n faker.name/last-name)
        (repeatedly n #(d/tempid :db.part/user))))


(defn subsets [xs]
  (rest
    (reduce (fn [s x] (into s (map #(conj % x) s))) [#{}] xs)))


(defn generate-users [n]
  (mapv #(hash-map :db/id %
                   :person/firstname %2
                   :person/lastname %3 
                   :user/email %4 
                   )
        (repeatedly n #(d/tempid :db.part/user))
        (repeatedly n faker.name/first-name)
        (repeatedly n faker.name/last-name)
        (repeatedly n faker.internet/email)))


(defn generate-rating [instructor]
  (hash-map :db/id (d/tempid :db.part/user)
            :rating/value (+ 1 (rand-int 4))
            :rating/instructor (:db/id instructor)
            :rating/comments (first (faker.lorem/paragraphs))
            ))


(defn add-user-ratings [user instructors]
  (let [instructor-coll (rand-nth (subsets instructors))
        ratings (mapv generate-rating instructor-coll)
        ratings-ids (mapv :db/id ratings)]
    (conj ratings {:db/id (:db/id user) :user/ratings ratings-ids})))


(defn generate-tx [{:keys [num-instructors num-users]}]

  (let [instructors (generate-instructors num-instructors)
        users (generate-users num-users)]
    (vec 
      (concat instructors users 
              (mapcat #(add-user-ratings % instructors) users)
              ))
    ))

; Stateful
; --------

(defn setup []
  (do
    (def uri "datomic:free://localhost:4334//myfitnessgurus")
    (d/delete-database uri)
    (d/create-database uri)
    (def conn (d/connect uri))
    @(d/transact conn schema)
    @(d/transact conn (generate-tx {:num-users 3 :num-instructors 9}))
    ))

(setup)

(defn find-attribute [conn keyword]
  (q '[:find ?attr 
       :in $ ?name 
       :where 
       [?attr :db/ident ?name]]
     (db conn) keyword))


(defn qes
  "Returns the entities returned by a query, assuming that
  all :find results are entity ids."
  [query db & args]
  (mapv (fn [items] (mapv (partial d/entity db) items)) 
        (apply q query db args)
        ))


(defn find-all-by [dbval attr]
  (map (comp (partial d/entity dbval) first)
       (q '[:find ?e 
            :in $ ?attr
            :where [?e ?attr]] 
          dbval attr)
       ))


(defn find-by
  "Returns the entities identified by attr and val."
  [dbval attr val]
  (map (comp (partial d/entity dbval) first)
       (q '[:find ?e
            :in $ ?attr ?val
            :where [?e ?attr ?val]]
          dbval attr val)))


(defn get-all-ratings []
  (find-all-by (db conn) :rating/value))


(defn get-all-instructors []
  (find-all-by (db conn) :instructor/gym))


(defn get-ratings [instructor]
  (find-by (db conn) :rating/instructor (:db/id instructor))) 


(defn get-avg-rating [instructor]
  (let [ratings (get-ratings instructor)
        num-ratings (count ratings) ]
    (if (= 0 num-ratings)
      0
      (/
       (reduce + (map :rating/value ratings))
       num-ratings))))
