(ns myfitnessgurus.views.layout
  (:use hiccup.form
        [hiccup.element :only [link-to]] 
        [hiccup.page :only [html5 include-js include-css]]))


(defn header []  
  [:div.navbar.navbar-fixed-top.navbar-inverse            
   [:div.navbar-inner 
    [:div.container

     ; collapsed menu button
     [:a.btn.btn-navbar {:data-toggle "collapse" :data-target ".nav-collapse"}
      (repeat 3 [:span.icon-bar])]

     ; brand
     (link-to {:class "brand"} "/" "MyFitnessGurus")

     ; menu
     [:ul.nav
      [:li (link-to "/" "Home")]
      [:li (link-to "/about" "About")]]
     ]]])


(defn footer []
  ;[:footer#footer.navbar.navbar-fixed-bottom 
  [:footer#footer.footer
   [:div.container
    [:hr]
    [:p.pull-right (link-to "#" "Back to top")]
    "MyFitnessGurus"]])


(defn base [& content]
  (html5 
    [:head
     [:title "Welcome to MyFitnessGurus"]
     (include-css "/css/screen.css")
     (include-css "/css/bootstrap.min.css")
     (include-css "/css/bootstrap-responsive.min.css")
     (include-js "//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js")
     (include-js "/js/core.js")]     
    [:body content]))

(defn common [& content]
  (base
    (header)
    [:div#wrap
     [:div#content.container content]
     [:div#push]
     ]
     (footer)
     ))
