(ns myfitnessgurus.util
  (:require [noir.io :as io]
            [markdown.core :as md]
            [clojure.pprint :as pprint]
            ))

(defn format-time
  "formats the time using SimpleDateFormat, the default format is
   \"dd MMM, yyyy\" and a custom one can be passed in as the second argument"
  ([time] (format-time time "dd MMM, yyyy"))
  ([time fmt]
    (.format (new java.text.SimpleDateFormat fmt) time)))

(defn md->html
  "reads a markdown file from public/md and returns an HTML string"
  [filename]
  (->> 
    (io/slurp-resource "md" filename)      
    (md/md-to-html-string)))

(defmacro defpp
  "Like def, but pretty prints the value of the var created"
  [name & more]
  `(do
     (def ~name ~@more)
     (pprint/pprint ~name)
     (var ~name)))

